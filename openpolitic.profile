<?php

/**
 * Implements hook_install_tasks_alter().
 */
function openpolitic_install_tasks_alter(&$tasks, $install_state) {
  $target_theme = 'transparency';
  $theme_settings  = &drupal_static('theme_get_setting', array());

  // Change the default theme
  if ($GLOBALS['theme'] != $target_theme) {
    unset($GLOBALS['theme']);
    drupal_static_reset();
    $GLOBALS['conf']['maintenance_theme'] = $target_theme;
    _drupal_maintenance_theme();
  }

  //$tasks['install_finished']['function'] = '_open_politic_install_finished';

   // The "Welcome" screen needs to come after the first two steps
  $new_task['install_welcome'] = array(
    'display' => TRUE,
    'display_name' => st('Welcome'),
    'type' => 'form',
    'run' => isset($install_state['parameters']['welcome']) ? INSTALL_TASK_SKIP : INSTALL_TASK_RUN_IF_REACHED,
  );
  $old_tasks = $tasks;
  $tasks = array_slice($old_tasks, 0, 2) + $new_task + array_slice($old_tasks, 2);

  $new_task['_openpolitic_encouraging_democracy'] = array(
    'display' => TRUE,
    'display_name' => st('Encouraging Democracy'),
    'type' => 'normal',
    'run' => isset($install_state['parameters']['democracy'])?INSTALL_TASK_SKIP:INSTALL_TASK_RUN_IF_REACHED,
  );
  $old_tasks = $tasks;
  $tasks = array_slice($old_tasks,0,11) + $new_task + array_slice($old_tasks,11);
  if($install_state['installation_finished'] == TRUE){
      drupal_set_message(st('Some data has been generated for the proper functioning of the distribution, take enable you to take look at their behavior before making changes'),'status');
  }
}

/**
 * Implements hook_form_alter().
 * Make the site_default_country field required for the correct operation of the module main location
 */
function openpolitic_form_install_configure_form_alter(&$form, &$form_state, $form_id) {
  $form['server_settings']['site_default_country']['#required'] = TRUE;
}

/**
 * Task callback: shows the welcome screen.
 */
function install_welcome($form, &$form_state, &$install_state) {
  drupal_set_title(st('Welcome to') . ' Open Politic' );

  //$message = st('Thank you for choosing Commerce Kickstart!') . '<br />';
  $message = st('Thank you for choosing the distribution ') . '<strong>Open Politic</strong><br />';
  $message .= '<p>' . st('This distribution installs and configure modules used build a website to help your community in a democracy process.') . '<p>';
  $message .= '<p>' . st('However, this distribution has additional modules not enabled and is recommended to activate your liking.') . '<p>';
  $message .= '<p> ' . st('We invite you to report any incident that occurs using the distribution by using the following link: ') .
      l('https://drupal.org/project/issues/openpolitic', 'https://drupal.org/project/issues/openpolitic', array('attributes' => array('target'=>'_blank'))) . '.</p>';

  $form = array();
  $form['welcome_message'] = array(
    '#markup' => $message,
  );
  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => st("Proceed with installation!"),
    '#weight' => 10,
  );
  return $form;
}

function install_welcome_submit($form, &$form_state) {
  global $install_state;

  $install_state['parameters']['welcome'] = 'done';
}

function _openpolitic_encouraging_democracy(&$install_state) {
  $translation_modules = array('entity_translation','i18n','l10n_client',
    'l10n_update','languageicons','locale');

  global $user;
  $values = array(
    'type' => 'page',
    'uid' => $user->uid,
    'status' => 1,
    'comment' => 0,
    'promote' => 0,
  );
  $page_entity = entity_create('node', $values);
  $page_wrapper = entity_metadata_wrapper('node', $page_entity);
  $page_wrapper->title->set('Who we are');
  $message = '<h2>Open Politic</h2>';
  $message .= '<p>Audience: Citizens, Communities & NGOs focused on strengthening democracy.</p>';
  $message .= '<p>Community Tool for generating a digital profile of the candidates where citizens can report actions taken by the candidate successes and failures.</p>';
  $message .= '<h3>Features:</h3>';
  $message .='<ul>
              <li>Ability to create political parties.</li>
              <li>Ability to create public job candidates belonging to a political party.</li>
              <li>Ability to store the candidates proposed.</li>
              <li>Possibility to compare candidates proposals on specific topics.</li>
              <li>Users can report achievements, failures, legal proceedings or sentencing processes of candidates or political parties, this reports are called references.</li>
              <li>Each citizens can vote like or dislike for one candidate or party.</li>
              <li>Ability to share the profile of a candidate, party or reference.</li>
              <li>Ability to make comments about a party, candidate or reference.</li>
              </ul>';
  $message.= '<h3>Replicating the experience:</h3>';
  $message.= '<p>Open Politic is a Drupal distribution that allows any person anywhere in the world, implementing a site for your local or national elections.</p>';
  $page_wrapper->body->set(array('value'=>$message , 'format' =>'full_html'));
  $page_wrapper->save();

  $arr_country = country_get_list();

  $country = $arr_country[variable_get('site_default_country', NULL)];
  $default_location = taxonomy_vocabulary_machine_name_load('location_of_government');
  $default_location_term = (object) array(
   'name' => $country,
   'description' => st('Country of Location Goverment'),
   'vid' => $default_location->vid,
  );
  taxonomy_term_save($default_location_term);
  variable_set('default_country',$country);

  $hierarchical_government = taxonomy_vocabulary_machine_name_load('hierarchical_government');
  $data_type = taxonomy_vocabulary_machine_name_load('data_type');
  $location_of_government = taxonomy_vocabulary_machine_name_load('location_of_government');
  $proposals = taxonomy_vocabulary_machine_name_load('proposals');
  $reference_type = taxonomy_vocabulary_machine_name_load('reference_type');
  taxonomy_term_save((object) array(
    'name' => 'President',
    'vid' => $hierarchical_government->vid,
  ));
  taxonomy_term_save((object) array(
    'name' => 'Congressman',
    'vid' => $hierarchical_government->vid,
  ));
  taxonomy_term_save((object) array(
    'name' => 'Article',
    'vid' => $data_type->vid,
  ));
  taxonomy_term_save((object) array(
    'name' => 'Audio',
    'vid' => $data_type->vid,
  ));
  taxonomy_term_save((object) array(
    'name' => 'Education',
    'vid' => $proposals->vid,
  ));
  taxonomy_term_save((object) array(
    'name' => 'Health',
    'vid' => $proposals->vid,
  ));
  taxonomy_term_save((object) array(
    'name' => 'Information',
    'vid' => $reference_type->vid,
  ));
  taxonomy_term_save((object) array(
    'name' => 'Accusation',
    'vid' => $reference_type->vid,
  ));
  $country = taxonomy_get_term_by_name(variable_get('default_country'));
  taxonomy_term_save((object) array(
    'name' => 'Dummy Town',
    'vid' => $location_of_government->vid,
    'parent' => $country[1]->tid,
  ));

  /*Adding demo Data*/
   _openpolitic_add_demo_data();

  $install_state['parameters']['democracy'] = 'done';

  //Setting default blocks for distro
  configuring_open_politic_blocks();

  //Configure share this module
   configure_sharethis_module();
  //Enable modules for translation if locate it's not English
  if($install_state['parameters']['locale'] != 'en') {
    //Enabling modules using module enable
    module_enable($translation_modules);
  }
  /*Enable theme*/
  theme_enable(array('transparency'));
  //Add wysiwyg for full_html format
  $settings = array(
    'default' => 1,
    'user_choose' => 0,
    'show_toggle' => 1,
    'theme' => 'advanced',
    'language' => 'es',
    'buttons' =>
    array(
      'default' =>
      array(
        'bold' => 1,
        'italic' => 1,
        'underline' => 1,
        'strikethrough' => 1,
        'justifyleft' => 1,
        'justifycenter' => 1,
        'justifyright' => 1,
        'bullist' => 1,
        'numlist' => 1,
        'outdent' => 1,
        'indent' => 1,
        'undo' => 1,
        'redo' => 1,
        'link' => 1,
        'unlink' => 1,
        'anchor' => 1,
        'cleanup' => 1,
        'formatselect' => 1,
        'cut' => 1,
        'copy' => 1,
        'paste' => 1,
        'removeformat' => 1,
      ),
      'advlink' => array(
        'advlink' => 1,
      ),
      'contextmenu' => array(
        'contextmenu' => 1,
      ),
      'inlinepopups' => array(
        'inlinepopups' => 1,
      ),
      'paste' => array(
        'pastetext' => 1,
        'pasteword' => 1,
        'selectall' => 1,
      ),
      'searchreplace' => array(
        'search' => 1,
        'replace' => 1,
      ),
      'table' => array(
        'tablecontrols' => 1,
      ),
      'xhtmlxtras' => array(
        'del' => 1,
      ),
      'advlist' => array(
        'advlist' => 1,
      ),
      'drupal' => array(
        'break' => 1,
      ),
    ),
    'toolbar_loc' => 'top',
    'toolbar_align' => 'left',
    'path_loc' => 'bottom',
    'resizing' => 1,
    'verify_html' => 1,
    'preformatted' => 0,
    'convert_fonts_to_spans' => 1,
    'remove_linebreaks' => 1,
    'apply_source_formatting' => 0,
    'paste_auto_cleanup_on_paste' => 1,
    'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
    'css_setting' => 'self',
    'css_path' => '',
    'css_classes' => '',
  );

  $fields = array(
    'format' => 'full_html',
    'editor' => 'tinymce',
    'settings' => serialize($settings),
  );

  db_insert('wysiwyg')->fields($fields)->execute();
  //Add wysiwyg for filtered_html format
  $settings = array(
    'default' => 1,
    'user_choose' => 0,
    'show_toggle' => 1,
    'theme' => 'advanced',
    'language' => 'es',
    'buttons' =>
    array(
      'default' =>
      array(
        'bold' => 1,
        'italic' => 1,
        'underline' => 1,
        'strikethrough' => 1,
        'justifyleft' => 1,
        'justifycenter' => 1,
        'justifyright' => 1,
        'bullist' => 1,
        'numlist' => 1,
        'outdent' => 1,
        'indent' => 1,
        'undo' => 1,
        'redo' => 1,
        'link' => 1,
        'unlink' => 1,
        'anchor' => 1,
        'cleanup' => 1,
        'formatselect' => 1,
        'cut' => 1,
        'copy' => 1,
        'paste' => 1,
        'removeformat' => 1,
      ),
    ),
    'toolbar_loc' => 'top',
    'toolbar_align' => 'left',
    'path_loc' => 'bottom',
    'resizing' => 1,
    'verify_html' => 1,
    'preformatted' => 0,
    'convert_fonts_to_spans' => 1,
    'remove_linebreaks' => 1,
    'apply_source_formatting' => 0,
    'paste_auto_cleanup_on_paste' => 1,
    'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
    'css_setting' => 'self',
    'css_path' => '',
    'css_classes' => '',
  );

  $fields = array(
    'format' => 'filtered_html',
    'editor' => 'tinymce',
    'settings' => serialize($settings),
  );

  db_insert('wysiwyg')->fields($fields)->execute();
  return st("Installation Completed");
}

function _openpolitic_add_demo_data(){
  /*Generating content*/
  create_partys();
  create_proposals();
  create_candidates();
  create_references();
  set_reference_and_proposals();
  /*Generating dummy like & dislike values*/
  /*Partys*/
  generate_dummy_votes('20','2','like');
  generate_dummy_votes('25','2','dislike');
  generate_dummy_votes('10','3','like');
  generate_dummy_votes('5','3','dislike');
  /*Candidate*/
  generate_dummy_votes('5','8','like');
  generate_dummy_votes('10','8','dislike');
  generate_dummy_votes('13','9','like');
  generate_dummy_votes('10','9','dislike');
  generate_dummy_votes('20','10','like');
  generate_dummy_votes('15','10','dislike');
  generate_dummy_votes('15','11','like');
  generate_dummy_votes('15','11','dislike');
}
function create_partys(){
  global $user;
  $values = array(
    'type' => 'party',
    'uid' => $user->uid,
    'status' => 1,
    'comment' => 0,
    'promote' => 0,
  );

  $party_entity = entity_create('node', $values);
  $party_wrapper = entity_metadata_wrapper('node', $party_entity);
  $party_wrapper->title->set('Party #1');
  $party_wrapper->field_full_name->set('Party #1');
  $party_wrapper->field_website->set(array('url'=>'www.partyone.org','title' => 'Party #1'));
  $party_wrapper->field_facebook->set(array('url'=>'www.facebook.com','title' => 'Facebook'));
  $party_wrapper->field_twitter->set(array('url'=>'www.twitter.com','title' => 'Twitter'));
  $party_wrapper->field_google_plus->set(array('url'=>'www.plus.google.com','title' => 'Google Plus'));
  $party_wrapper->field_president ->set('President Party #1');
  $party_wrapper->field_address->set('Address 2 Street 2 123 Robles Robles');
  $party_wrapper->field_email->set('contact@partyone.org');
  $party_wrapper->field_phone->set('12-34-56-78');
  $party_wrapper->field_foundation_date->set(strtotime("now"));
  $filepath = DRUPAL_ROOT.'/profiles/openpolitic/images/partyone.jpg';
  $filename = 'partyone.jpg';
  $image = file_get_contents($filepath);
  $file = file_save_data($image, 'public://' . $filename, FILE_EXISTS_RENAME);
  $party_wrapper->field_logo->set((array) $file);
  $file = NULL;
  $party_wrapper->save();

  $party_entity = entity_create('node', $values);
  $party_wrapper = entity_metadata_wrapper('node', $party_entity);
  $party_wrapper->title->set('Party #2');
  $party_wrapper->field_full_name->set('Party #2');
  $party_wrapper->field_website->set(array('url'=>'www.partytwo.org','title' => 'Party #2'));
  $party_wrapper->field_facebook->set(array('url'=>'www.facebook.com','title' => 'Facebook'));
  $party_wrapper->field_twitter->set(array('url'=>'www.twitter.com','title' => 'Twitter'));
  $party_wrapper->field_google_plus->set(array('url'=>'www.plus.google.com','title' => 'Google Plus'));
  $party_wrapper->field_president ->set('President Party #1');
  $party_wrapper->field_address->set('First Avenue, Second Street 456 Mars');
  $party_wrapper->field_email->set('contact@partytwo.org');
  $party_wrapper->field_phone->set('12-53-12-75');
  $party_wrapper->field_foundation_date->set(strtotime("now"));
  $filepath = DRUPAL_ROOT.'/profiles/openpolitic/images/partytwo.jpg';
  $filename = 'partytwo.jpg';
  $image = file_get_contents($filepath);
  $file = file_save_data($image, 'public://' . $filename, FILE_EXISTS_RENAME);
  $party_wrapper->field_logo->set((array) $file);
  $file = NULL;
  $party_wrapper->save();
}

function create_proposals(){
  global $user;
  $values = array(
    'type' => 'proposals',
    'uid' => $user->uid,
    'status' => 1,
    'comment' => 0,
    'promote' => 0,
  );
  $proposal_entity = entity_create('node', $values);
  $proposal_wrapper = entity_metadata_wrapper('node', $proposal_entity);
  $proposal_wrapper->title->set('Proposal #1');
  $proposal_wrapper->field_type->set(intval(6));
  $proposal_wrapper->body->set(array('value'=>'The lorem ipsum text is typically a section of a Latin text by Cicero with words altered, added and removed that make it nonsensical in meaning and not proper Latin.
  '));
  $proposal_wrapper->save();

  $proposal_entity = entity_create('node', $values);
  $proposal_wrapper = entity_metadata_wrapper('node', $proposal_entity);
  $proposal_wrapper->title->set('Proposal #2');
  $proposal_wrapper->field_type->set(intval(7));
  $proposal_wrapper->body->set(array('value'=>'In publishing and graphic design, Lorem ipsum is placeholder text (filler text) commonly used to demonstrate the graphics elements of a document or visual presentation, such as font, typography, and layout, by removing the distraction of meaningful content.'));
  $proposal_wrapper->save();

  $proposal_entity = entity_create('node', $values);
  $proposal_wrapper = entity_metadata_wrapper('node', $proposal_entity);
  $proposal_wrapper->title->set('Proposal #3');
  $proposal_wrapper->field_type->set(intval(7));
  $proposal_wrapper->body->set(array('value'=>'The lorem ipsum text is typically a section of a Latin text by Cicero with words altered, added and removed that make it nonsensical in meaning and not proper Latin.
  '));
  $proposal_wrapper->save();

  $proposal_entity = entity_create('node', $values);
  $proposal_wrapper = entity_metadata_wrapper('node', $proposal_entity);
  $proposal_wrapper->title->set('Proposal #4');
  $proposal_wrapper->field_type->set(intval(6));
  $proposal_wrapper->body->set(array('value'=>'In publishing and graphic design, Lorem ipsum is placeholder text (filler text) commonly used to demonstrate the graphics elements of a document or visual presentation, such as font, typography, and layout, by removing the distraction of meaningful content.'));
  $proposal_wrapper->save();

}

function create_candidates(){
  global $user;
  $values = array(
    'type' => 'candidate',
    'uid' => $user->uid,
    'status' => 1,
    'comment' => 0,
    'promote' => 0,
  );
  $location_of_government = taxonomy_vocabulary_machine_name_load('location_of_government');
  $candidate_entity = entity_create('node', $values);
  $candidate_wrapper = entity_metadata_wrapper('node', $candidate_entity);
  $candidate_wrapper->title->set('Candidate President #1');
  $candidate_wrapper->body->set(array('value'=>'Even though "lorem ipsum" may arouse curiosity because of its resemblance to classical Latin, it is not intended to have meaning. If text is comprehensible in a document, people tend to focus on the textual content rather than upon overall presentation.'));
  $candidate_wrapper->field_party->set(intval(2));
  $candidate_wrapper->field_hierarchical_government->set(intval(2));
  $candidate_wrapper->field_location_of_government->set(array(1));
  $candidate_wrapper->field_candidate_->set('1');
  $candidate_wrapper->field_website->set(array('url'=>'www.partyone.org','title' => 'Party #1'));
  $candidate_wrapper->field_facebook->set(array('url'=>'www.facebook.com','title' => 'Facebook'));
  $candidate_wrapper->field_twitter->set(array('url'=>'www.twitter.com','title' => 'Twitter'));
  $candidate_wrapper->field_google_plus->set(array('url'=>'www.plus.google.com','title' => 'Google Plus'));
  $candidate_wrapper->field_email->set('candidate_president@partyone.org');
  $candidate_wrapper->field_phone->set('12-34-56-78');
  $filepath = DRUPAL_ROOT.'/profiles/openpolitic/images/candidate.jpg';
  $filename = 'candidate.jpg';
  $image = file_get_contents($filepath);
  $file = file_save_data($image, 'public://' . $filename, FILE_EXISTS_RENAME);
  $candidate_wrapper->field_image->set((array) $file);
  $file = NULL;
  $candidate_wrapper->save();

  $candidate_entity = entity_create('node', $values);
  $candidate_wrapper = entity_metadata_wrapper('node', $candidate_entity);
  $candidate_wrapper->title->set('Candidate Congressman #1');
  $candidate_wrapper->body->set(array('value'=>'Therefore publishers use lorem ipsum when displaying a typeface or design elements and page layout in order to direct the focus to the publication style and not the meaning of the text. In spite of its basis in Latin'));
  $candidate_wrapper->field_party->set(intval(2));
  $candidate_wrapper->field_hierarchical_government->set(intval(3));
  $candidate_wrapper->field_location_of_government->set(array(10));
  $candidate_wrapper->field_candidate_->set('2');
  $candidate_wrapper->field_website->set(array('url'=>'www.partyone.org','title' => 'Party #1'));
  $candidate_wrapper->field_facebook->set(array('url'=>'www.facebook.com','title' => 'Facebook'));
  $candidate_wrapper->field_twitter->set(array('url'=>'www.twitter.com','title' => 'Twitter'));
  $candidate_wrapper->field_google_plus->set(array('url'=>'www.plus.google.com','title' => 'Google Plus'));
  $candidate_wrapper->field_email->set('ccandidate_congressman@partyone.org');
  $candidate_wrapper->field_phone->set('12-34-56-78');
  $filepath = DRUPAL_ROOT.'/profiles/openpolitic/images/candidate.jpg';
  $filename = 'candidate.jpg';
  $image = file_get_contents($filepath);
  $file = file_save_data($image, 'public://' . $filename, FILE_EXISTS_RENAME);
  $candidate_wrapper->field_image->set((array) $file);
  $file = NULL;
  $candidate_wrapper->save();

  $candidate_entity = entity_create('node', $values);
  $candidate_wrapper = entity_metadata_wrapper('node', $candidate_entity);
  $candidate_wrapper->title->set('Candidate President #2');
  $candidate_wrapper->body->set(array('value'=>'Even though "lorem ipsum" may arouse curiosity because of its resemblance to classical Latin, it is not intended to have meaning. If text is comprehensible in a document, people tend to focus on the textual content rather than upon overall presentation.'));
  $candidate_wrapper->field_party->set(intval(3));
  $candidate_wrapper->field_hierarchical_government->set(intval(2));
  $candidate_wrapper->field_location_of_government->set(array(1));
  $candidate_wrapper->field_candidate_->set('1');
  $candidate_wrapper->field_website->set(array('url'=>'www.partytwo.org','title' => 'Party #2'));
  $candidate_wrapper->field_facebook->set(array('url'=>'www.facebook.com','title' => 'Facebook'));
  $candidate_wrapper->field_twitter->set(array('url'=>'www.twitter.com','title' => 'Twitter'));
  $candidate_wrapper->field_google_plus->set(array('url'=>'www.plus.google.com','title' => 'Google Plus'));
  $candidate_wrapper->field_email->set('candidate_president@partytwo.org');
  $candidate_wrapper->field_phone->set('12-34-56-78');
  $filepath = DRUPAL_ROOT.'/profiles/openpolitic/images/candidate.jpg';
  $filename = 'candidate.jpg';
  $image = file_get_contents($filepath);
  $file = file_save_data($image, 'public://' . $filename, FILE_EXISTS_RENAME);
  $candidate_wrapper->field_image->set((array) $file);
  $file = NULL;
  $candidate_wrapper->save();

  $candidate_entity = entity_create('node', $values);
  $candidate_wrapper = entity_metadata_wrapper('node', $candidate_entity);
  $candidate_wrapper->title->set('Candidate Congressman #2');
  $candidate_wrapper->body->set(array('value'=>'Therefore publishers use lorem ipsum when displaying a typeface or design elements and page layout in order to direct the focus to the publication style and not the meaning of the text. In spite of its basis in Latin'));
  $candidate_wrapper->field_party->set(intval(3));
  $candidate_wrapper->field_hierarchical_government->set(intval(3));
  $candidate_wrapper->field_location_of_government->set(array(10));
  $candidate_wrapper->field_candidate_->set('2');
  $candidate_wrapper->field_website->set(array('url'=>'www.partytwo.org','title' => 'Party #2'));
  $candidate_wrapper->field_facebook->set(array('url'=>'www.facebook.com','title' => 'Facebook'));
  $candidate_wrapper->field_twitter->set(array('url'=>'www.twitter.com','title' => 'Twitter'));
  $candidate_wrapper->field_google_plus->set(array('url'=>'www.plus.google.com','title' => 'Google Plus'));
  $candidate_wrapper->field_email->set('ccandidate_congressman@partytwo.org');
  $candidate_wrapper->field_phone->set('12-34-56-78');
  $filepath = DRUPAL_ROOT.'/profiles/openpolitic/images/candidate.jpg';
  $filename = 'candidate.jpg';
  $image = file_get_contents($filepath);
  $file = file_save_data($image, 'public://' . $filename, FILE_EXISTS_RENAME);
  $candidate_wrapper->field_image->set((array) $file);
  $file = NULL;
  $candidate_wrapper->save();

}

function create_references(){
  global $user;
  $values = array(
    'type' => 'reference',
    'uid' => $user->uid,
    'status' => 1,
    'comment' => 0,
    'promote' => 0,
  );
  $reference_entity = entity_create('node', $values);
  $reference_wrapper = entity_metadata_wrapper('node', $reference_entity);
  $reference_wrapper->title->set('Reference #1');
  //$reference_wrapper->field_video->set(set(intval(2)));
  $reference_wrapper->field_source->set(array('url'=>'http://www.nytimes.com/2014/03/03/world/europe/ukraine.html?hp&_r=0','title' => 'Reference #1'));
  $reference_wrapper->field_reference_type->set(intval(8));
  $reference_wrapper->field_related_to->set(array(8));
  $reference_wrapper->save();

  $reference_entity = entity_create('node', $values);
  $reference_wrapper = entity_metadata_wrapper('node', $reference_entity);
  $reference_wrapper->title->set('Reference #2');
  //$reference_wrapper->field_video->set(set(intval(2)));
  $reference_wrapper->field_source->set(array('url'=>'http://www.economist.com/business-finance','title' => 'Reference #2'));
  $reference_wrapper->field_reference_type->set(intval(9));
  $reference_wrapper->field_related_to->set(array(3));
  $reference_wrapper->save();

  $reference_entity = entity_create('node', $values);
  $reference_wrapper = entity_metadata_wrapper('node', $reference_entity);
  $reference_wrapper->title->set('Reference #3');
  //$reference_wrapper->field_video->set(set(intval(2)));
  $reference_wrapper->field_source->set(array('url'=>'http://www.economist.com/business-finance','title' => 'Reference #3'));
  $reference_wrapper->field_reference_type->set(intval(8));
  $reference_wrapper->field_related_to->set(array(2));
  $reference_wrapper->save();

  $reference_entity = entity_create('node', $values);
  $reference_wrapper = entity_metadata_wrapper('node', $reference_entity);
  $reference_wrapper->title->set('Reference #4');
  //$reference_wrapper->field_video->set(set(intval(2)));
  $reference_wrapper->field_source->set(array('url'=>'http://www.nbc.com/','title' => 'Reference #4'));
  $reference_wrapper->field_reference_type->set(intval(8));
  $reference_wrapper->field_related_to->set(array(11));
  $reference_wrapper->save();
}
function set_reference_and_proposals(){
  $candidate_wrapper = entity_metadata_wrapper('node',8);
  $candidate_wrapper->field_proposals->set(array(4));
  $candidate_wrapper->save();

  $candidate_wrapper = entity_metadata_wrapper('node',9);
  $candidate_wrapper->field_proposals->set(array(5));
  $candidate_wrapper->save();

  $candidate_wrapper = entity_metadata_wrapper('node',10);
  $candidate_wrapper->field_proposals->set(array(6));
  $candidate_wrapper->save();

  $candidate_wrapper = entity_metadata_wrapper('node',11);
  $candidate_wrapper->field_proposals->set(array(7));
  $candidate_wrapper->save();
}
function generate_dummy_votes($quantity,$nid,$action){
  $vote = array(
    'entity_type' => 'node',
    'entity_id' => $nid,
    'value' => 1,
    'value_type' => 'percent',
    'tag' => $action,
    'uid' => 0,
    'timestamp' => time(),
    'vote_source' => '127.0.0.1',
    );

  $query = db_insert('votingapi_vote')->fields(array('entity_type', 'entity_id', 'value', 'value_type', 'tag', 'uid','timestamp','vote_source'));
  for ($i=0; $i < $quantity; $i++) {
    $query->values($vote);
  }
  $query->execute();
}
function configuring_open_politic_blocks(){
  $default_theme = variable_get('theme_default', 'transparency');
  $admin_theme = variable_get('admin_theme', 'seven');
  $blocks = array(
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'search',
      'delta' => 'form',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => -10,
      'region' => 'branding',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'footer_message',
      'delta' => 'footer_message',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 9,
      'region' => 'footer_first',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'open_politic_helper',
      'delta' => 'powered-by',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 10,
      'region' => 'footer_first',
      'pages' => '',
      'cache' => -1,
    ),
        array(
      'module' => 'node',
      'delta' => 'recent',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 10,
      'region' => 'dashboard_main',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'help',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'help',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'user',
      'delta' => 'login',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 10,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'user',
      'delta' => 'new',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'dashboard_sidebar',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'search',
      'delta' => 'form',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => -10,
      'region' => 'dashboard_sidebar',
      'pages' => '',
      'cache' => -1,
    ),
  );
  $query = db_insert('block')->fields(array('module', 'delta', 'theme', 'status', 'weight', 'region', 'pages', 'cache'));
  foreach ($blocks as $block) {
    $query->values($block);
  }
  $query->execute();
}
function configure_sharethis_module(){
  variable_set('sharethis_button_option', 'stbc_large');
    variable_set('sharethis_node_option', 'candidate,party,reference,0,0');
    variable_set('sharethis_location', 'blocks');
    variable_set('sharethis_service_option', '"Facebook:facebook","Tweet:twitter","Google +:googleplus","Email:email"');
}
