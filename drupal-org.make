api = 2
core = 7.x

projects[admin_menu][subdir] = contrib
projects[admin_menu][version] = 3.0-rc4

projects[advanced_help][subdir] = contrib
projects[advanced_help][version] = 1.1

projects[better_exposed_filters][subdir] = contrib
projects[better_exposed_filters][version] = 3.0-beta4

projects[block_class][subdir] = contrib
projects[block_class][version] = 2.1

projects[bundle_copy][subdir] = contrib
projects[bundle_copy][version] = 1.1

projects[charts][subdir] = contrib
projects[charts][version] = 2.0-rc1

projects[coffee][subdir] = contrib
projects[coffee][version] = 2.2

projects[context][subdir] = contrib
projects[context][version] = 3.2

projects[ctools][subdir] = contrib
projects[ctools][version] = 1.4

projects[date][subdir] = contrib
projects[date][version] = 2.7

projects[delta][subdir] = contrib
projects[delta][version] = 3.x-dev

projects[disable_breadcrumbs][subdir] = contrib
projects[disable_breadcrumbs][version] = 1.3

projects[ds][subdir] = contrib
projects[ds][version] = 2.6

projects[email][subdir] = contrib
projects[email][version] = 1.3

projects[entity][subdir] = contrib
projects[entity][version] = 1.5

projects[entityreference][subdir] = contrib
projects[entityreference][version] = 1.1

projects[features][subdir] = contrib
projects[features][version] = 2.0

projects[field_group][subdir] = contrib
projects[field_group][version] = 1.3

projects[flag][subdir] = contrib
projects[flag][version] = 3.3

projects[fontyourface][subdir] = contrib
projects[fontyourface][version] = 2.8

projects[footer_message][subdir] = contrib
projects[footer_message][version] = 1.1

projects[globalredirect][subdir] = contrib
projects[globalredirect][version] = 1.5

projects[google_analytics][subdir] = contrib
projects[google_analytics][version] = 1.4

projects[hierarchical_select][subdir] = contrib
projects[hierarchical_select][version] = 3.0-alpha6

projects[i18n][subdir] = contrib
projects[i18n][version] = 1.11

projects[inline_entity_form][subdir] = contrib
projects[inline_entity_form][version] = 1.5

projects[l10n_client][subdir] = contrib
projects[l10n_client][version] = 1.3

projects[l10n_update][subdir] = contrib
projects[l10n_update][version] = 1.0

projects[languageicons][subdir] = contrib
projects[languageicons][version] = 1.1

projects[libraries][subdir] = contrib
projects[libraries][version] = 2.2

projects[likedislike][subdir] = contrib
projects[likedislike][version] = 1.0

projects[link][subdir] = contrib
projects[link][version] = 1.2

projects[link_iframe_formatter][subdir] = contrib
projects[link_iframe_formatter][version] = 1.1

projects[messageclose][subdir] = contrib
projects[messageclose][version] = 1.5

projects[metatag][subdir] = contrib
projects[metatag][version] = 1.0-beta9

projects[module_filter][subdir] = contrib
projects[module_filter][version] = 2.0-alpha2

projects[nocurrent_pass][subdir] = contrib
projects[nocurrent_pass][version] = 1.0

projects[node_clone][subdir] = contrib
projects[node_clone][version] = 1.0-rc2

projects[omega_tools][subdir] = contrib
projects[omega_tools][version] = 3.0-rc4

projects[panels][subdir] = contrib
projects[panels][version] = 3.4

projects[pathauto][subdir] = contrib
projects[pathauto][version] = 1.2

projects[pathologic][subdir] = contrib
projects[pathologic][version] = 2.12

projects[phone][subdir] = contrib
projects[phone][version] = 1.0-beta1

projects[popup][subdir] = contrib
projects[popup][version] = 1.3

projects[responsive_menus][subdir] = contrib
projects[responsive_menus][version] = 1.5

projects[sharethis][subdir] = contrib
projects[sharethis][version] = 2.6

projects[taxonomy_manager][subdir] = contrib
projects[taxonomy_manager][version] = 1.0

projects[title][subdir] = contrib
projects[title][version] = 1.0-alpha7

projects[token][subdir] = contrib
projects[token][version] = 1.5

projects[variable][subdir] = contrib
projects[variable][version] = 2.5

projects[views][subdir] = contrib
projects[views][version] = 3.8

projects[views_matrix][subdir] = contrib
projects[views_matrix][version] = 1.x-dev

projects[votingapi][subdir] = contrib
projects[votingapi][version] = 2.11

projects[wysiwyg][subdir] = contrib
projects[wysiwyg][version] = 2.2

projects[youtube][subdir] = contrib
projects[youtube][version] = 1.2

projects[entity_translation][subdir] = contrib
projects[entity_translation][version] = 1.0-beta3

projects[hybridauth][subdir] = contrib
projects[hybridauth][version] = 2.8


;libraries[colorbox][download][type] = git
;libraries[colorbox][download][url] = https://github.com/jackmoore/colorbox.git
