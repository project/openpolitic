<?php
/**
 * @file
 * open_politic_structure.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function open_politic_structure_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_candidacy_data|node|candidate|form';
  $field_group->group_name = 'group_candidacy_data';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'candidate';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Candidacy data',
    'weight' => '3',
    'children' => array(
      0 => 'field_party',
      1 => 'field_hierarchical_government',
      2 => 'field_location_of_government',
      3 => 'field_candidate_',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_candidacy_data|node|candidate|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_organinzation|node|party|form';
  $field_group->group_name = 'group_organinzation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'party';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Organization',
    'weight' => '9',
    'children' => array(
      0 => 'field_president',
      1 => 'field_address',
      2 => 'field_email',
      3 => 'field_phone',
      4 => 'field_foundation_date',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Organization',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_organinzation|node|party|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_personal_data|node|candidate|form';
  $field_group->group_name = 'group_personal_data';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'candidate';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Personal data',
    'weight' => '4',
    'children' => array(
      0 => 'field_facebook',
      1 => 'field_twitter',
      2 => 'field_google_plus',
      3 => 'field_website',
      4 => 'field_email',
      5 => 'field_phone',
      6 => 'field_cv',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_personal_data|node|candidate|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_proposals|node|candidate|form';
  $field_group->group_name = 'group_proposals';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'candidate';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Proposals',
    'weight' => '5',
    'children' => array(
      0 => 'field_proposals',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_proposals|node|candidate|form'] = $field_group;

  return $export;
}
