<?php
/**
 * @file
 * open_politic_structure.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function open_politic_structure_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|candidate|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'candidate';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'share_this' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'links' => array(
      'weight' => '16',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '17',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'field_party' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => t('Party'),
        ),
      ),
    ),
    'field_hierarchical_government' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => t('Hierarchical government'),
        ),
      ),
    ),
    'field_location_of_government' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => t('Location of government'),
        ),
      ),
    ),
    'field_email' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => t('Email'),
        ),
      ),
    ),
    'field_phone' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => t('Phone'),
        ),
      ),
    ),
  );
  $export['node|candidate|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|candidate|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'candidate';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h2',
        'class' => '',
      ),
    ),
    'node_link' => array(
      'weight' => '8',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'field_hierarchical_government' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => t('Hierarchical government'),
        ),
      ),
    ),
    'field_location_of_government' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => t('Location of government'),
        ),
      ),
    ),
  );
  $export['node|candidate|teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|party|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'party';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'share_this' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '8',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'links' => array(
      'weight' => '15',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '16',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'field_president' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => t('President'),
        ),
      ),
    ),
    'field_address' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => t('Address'),
        ),
      ),
    ),
    'field_email' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => t('Email'),
        ),
      ),
    ),
    'field_phone' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => t('Phone'),
        ),
      ),
    ),
    'field_foundation_date' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => t('Foundation date'),
        ),
      ),
    ),
  );
  $export['node|party|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|proposals|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'proposals';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|proposals|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|reference|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'reference';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'share_this' => array(
      'weight' => '8',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'links' => array(
      'weight' => '9',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|reference|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function open_politic_structure_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'contributor_menu_popup';
  $ds_field->label = 'Contributor menu popup';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'open_politic_helper|contributor_menu_popup',
    'block_render' => '1',
  );
  $export['contributor_menu_popup'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'share_this';
  $ds_field->label = 'Share this';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'comment' => 'comment',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'sharethis|sharethis_block',
    'block_render' => '3',
  );
  $export['share_this'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function open_politic_structure_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|candidate|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'candidate';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_image',
        1 => 'share_this',
        2 => 'group_like_dislike',
        3 => 'field_facebook',
        4 => 'like',
        5 => 'field_twitter',
        6 => 'dislike',
        7 => 'field_google_plus',
      ),
      'right' => array(
        8 => 'title',
        9 => 'field_website',
        10 => 'field_party',
        11 => 'field_cv',
        12 => 'field_phone',
        13 => 'field_email',
        14 => 'field_hierarchical_government',
        15 => 'field_location_of_government',
        16 => 'field_candidate_',
        17 => 'body',
      ),
      'footer' => array(
        18 => 'links',
        19 => 'comments',
      ),
    ),
    'fields' => array(
      'field_image' => 'left',
      'share_this' => 'left',
      'group_like_dislike' => 'left',
      'field_facebook' => 'left',
      'like' => 'left',
      'field_twitter' => 'left',
      'dislike' => 'left',
      'field_google_plus' => 'left',
      'title' => 'right',
      'field_website' => 'right',
      'field_party' => 'right',
      'field_cv' => 'right',
      'field_phone' => 'right',
      'field_email' => 'right',
      'field_hierarchical_government' => 'right',
      'field_location_of_government' => 'right',
      'field_candidate_' => 'right',
      'body' => 'right',
      'links' => 'footer',
      'comments' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|candidate|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|candidate|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'candidate';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_image',
      ),
      'right' => array(
        1 => 'title',
        2 => 'field_party',
        3 => 'field_hierarchical_government',
        4 => 'field_location_of_government',
        5 => 'field_candidate_',
        6 => 'like',
        7 => 'dislike',
        8 => 'node_link',
      ),
    ),
    'fields' => array(
      'field_image' => 'left',
      'title' => 'right',
      'field_party' => 'right',
      'field_hierarchical_government' => 'right',
      'field_location_of_government' => 'right',
      'field_candidate_' => 'right',
      'like' => 'right',
      'dislike' => 'right',
      'node_link' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'hide_page_title' => '0',
    'page_option_title' => '',
  );
  $export['node|candidate|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|party|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'party';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_logo',
        1 => 'share_this',
        2 => 'group_like_dislike',
        3 => 'like',
        4 => 'dislike',
        5 => 'field_facebook',
        6 => 'field_twitter',
        7 => 'field_google_plus',
      ),
      'right' => array(
        8 => 'title',
        9 => 'field_website',
        10 => 'field_president',
        11 => 'field_address',
        12 => 'field_email',
        13 => 'field_phone',
        14 => 'field_foundation_date',
      ),
      'footer' => array(
        15 => 'links',
        16 => 'comments',
      ),
    ),
    'fields' => array(
      'field_logo' => 'left',
      'share_this' => 'left',
      'group_like_dislike' => 'left',
      'like' => 'left',
      'dislike' => 'left',
      'field_facebook' => 'left',
      'field_twitter' => 'left',
      'field_google_plus' => 'left',
      'title' => 'right',
      'field_website' => 'right',
      'field_president' => 'right',
      'field_address' => 'right',
      'field_email' => 'right',
      'field_phone' => 'right',
      'field_foundation_date' => 'right',
      'links' => 'footer',
      'comments' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|party|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|proposals|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'proposals';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title',
        1 => 'field_type',
        2 => 'body',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'field_type' => 'header',
      'body' => 'header',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|proposals|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|reference|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'reference';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'flag_report_abusive_content',
        1 => 'title',
        2 => 'field_related_to',
      ),
      'left' => array(
        3 => 'field_reference_type',
      ),
      'right' => array(
        4 => 'dislike',
        5 => 'like',
      ),
      'footer' => array(
        6 => 'field_video',
        7 => 'field_source',
        8 => 'share_this',
        9 => 'links',
        10 => 'comments',
      ),
    ),
    'fields' => array(
      'flag_report_abusive_content' => 'header',
      'title' => 'header',
      'field_related_to' => 'header',
      'field_reference_type' => 'left',
      'dislike' => 'right',
      'like' => 'right',
      'field_video' => 'footer',
      'field_source' => 'footer',
      'share_this' => 'footer',
      'links' => 'footer',
      'comments' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|reference|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|reference|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'reference';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'title',
        1 => 'field_video',
        2 => 'field_source',
        3 => 'field_reference_type',
        4 => 'path',
        5 => 'metatags',
      ),
      'right' => array(
        6 => 'field_related_to',
      ),
      'hidden' => array(
        7 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'field_video' => 'left',
      'field_source' => 'left',
      'field_reference_type' => 'left',
      'path' => 'left',
      'metatags' => 'left',
      'field_related_to' => 'right',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|reference|form'] = $ds_layout;

  return $export;
}
