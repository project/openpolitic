<?php
/**
* Implementation of hook_default_page_manager_pages().
*/
function open_politic_structure_default_page_manager_pages() {
  $pages = array();
  $path = drupal_get_path('module', 'open_politic_structure') . '/pages';
  //$files = drupal_system_listing('/\.inc$', $path, 'name', 0);
  $files = drupal_system_listing('/\.inc$/', $path, 'name', 0);
  foreach ($files as $file) {
    include_once $path . '/' . $file->filename;
    $pages[$page->name] = $page;
  }
  return $pages;
}
?>
