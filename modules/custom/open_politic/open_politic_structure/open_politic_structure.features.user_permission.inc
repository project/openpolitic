<?php
/**
 * @file
 * open_politic_structure.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function open_politic_structure_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'like comment'.
  $permissions['like comment'] = array(
    'name' => 'like comment',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'likedislike',
  );

  // Exported permission: 'like node article'.
  $permissions['like node article'] = array(
    'name' => 'like node article',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'likedislike',
  );

  // Exported permission: 'like node candidate'.
  $permissions['like node candidate'] = array(
    'name' => 'like node candidate',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'likedislike',
  );

  // Exported permission: 'like node page'.
  $permissions['like node page'] = array(
    'name' => 'like node page',
    'roles' => array(),
    'module' => 'likedislike',
  );

  // Exported permission: 'like node party'.
  $permissions['like node party'] = array(
    'name' => 'like node party',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'likedislike',
  );

  // Exported permission: 'like node proposals'.
  $permissions['like node proposals'] = array(
    'name' => 'like node proposals',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'likedislike',
  );

  // Exported permission: 'like node reference'.
  $permissions['like node reference'] = array(
    'name' => 'like node reference',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'likedislike',
  );

  // Exported permission: 'manage like dislike'.
  $permissions['manage like dislike'] = array(
    'name' => 'manage like dislike',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'likedislike',
  );

  // Exported permission: 'view likes article'.
  $permissions['view likes article'] = array(
    'name' => 'view likes article',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'likedislike',
  );

  // Exported permission: 'view likes candidate'.
  $permissions['view likes candidate'] = array(
    'name' => 'view likes candidate',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'likedislike',
  );

  // Exported permission: 'view likes comment'.
  $permissions['view likes comment'] = array(
    'name' => 'view likes comment',
    'roles' => array(),
    'module' => 'likedislike',
  );

  // Exported permission: 'view likes page'.
  $permissions['view likes page'] = array(
    'name' => 'view likes page',
    'roles' => array(),
    'module' => 'likedislike',
  );

  // Exported permission: 'view likes party'.
  $permissions['view likes party'] = array(
    'name' => 'view likes party',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'likedislike',
  );

  // Exported permission: 'view likes proposals'.
  $permissions['view likes proposals'] = array(
    'name' => 'view likes proposals',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'likedislike',
  );

  // Exported permission: 'view likes reference'.
  $permissions['view likes reference'] = array(
    'name' => 'view likes reference',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'likedislike',
  );

  return $permissions;
}
