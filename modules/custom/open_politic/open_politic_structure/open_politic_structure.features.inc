<?php
/**
 * @file
 * open_politic_structure.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function open_politic_structure_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "delta" && $api == "delta") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }

  if ($module == 'page_manager' && $api == 'pages_default') {
    return array('version' => 1);
  }
}

/**
 * Implements hook_views_api().
 */
function open_politic_structure_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function open_politic_structure_flag_default_flags() {
  $flags = array();
  // Exported flag: "Report abusive comment".
  $flags['report_abusive_comment'] = array(
    'entity_type' => 'comment',
    'title' => 'Report abusive comment',
    'global' => 0,
    'types' => array(
      0 => 'comment_node_candidate',
      1 => 'comment_node_party',
      2 => 'comment_node_proposals',
      3 => 'comment_node_reference',
    ),
    'flag_short' => 'Report abusive content',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove abusive content',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'token' => 0,
    ),
    'show_as_field' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'api_version' => 3,
    'module' => 'open_politic_structure',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Reportar contenido abusivo".
  $flags['report_abusive_content'] = array(
    'entity_type' => 'node',
    'title' => 'Report abusive content',
    'global' => 0,
    'types' => array(
      0 => 'reference',
    ),
    'flag_short' => 'Report abusive content',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove abusive content',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 0,
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'token' => 0,
      'revision' => 0,
    ),
    'show_as_field' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'api_version' => 3,
    'module' => 'open_politic_structure',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}

/**
 * Implements hook_image_default_styles().
 */
function open_politic_structure_image_default_styles() {
  $styles = array();

  // Exported image style: candidates_thumb.
  $styles['candidates_thumb'] = array(
    'name' => 'candidates_thumb',
    'effects' => array(
      5 => array(
        'label' => 'Escala',
        'help' => 'El escalado mantiene la relación de proporciones de la imagen original. Si sólo se especifica una dimensión, la otra se calculará.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 180,
          'height' => 180,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: miniature.
  $styles['miniature'] = array(
    'name' => 'miniature',
    'effects' => array(
      1 => array(
        'label' => 'Redimensionar',
        'help' => 'El redimensionado hará que las imágenes tomen unas dimensiones precisas. Esto puede hacer que las imágenes se estrechen o estiren desproporcionadamente.',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => 50,
          'height' => 40,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: parties.
  $styles['parties'] = array(
    'name' => 'parties',
    'effects' => array(
      2 => array(
        'label' => 'Escalar y recortar',
        'help' => 'Escalar y recortar mantendrán la relación de proporciones de la imagen original y se desechará lo que sobre por el lado mayor. Esto es especialmente útil para crear miniaturas perfectamente cuadradas sin deformar la imagen.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 220,
          'height' => 120,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: parties_thumb.
  $styles['parties_thumb'] = array(
    'name' => 'parties_thumb',
    'effects' => array(
      3 => array(
        'label' => 'Escalar y recortar',
        'help' => 'Escalar y recortar mantendrán la relación de proporciones de la imagen original y se desechará lo que sobre por el lado mayor. Esto es especialmente útil para crear miniaturas perfectamente cuadradas sin deformar la imagen.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 180,
          'height' => 100,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function open_politic_structure_node_info() {
  $items = array(
    'candidate' => array(
      'name' => t('Candidate'),
      'base' => 'node_content',
      'description' => t('Use this type of content to create a candidate belonging to a party and aspirant for public office'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'party' => array(
      'name' => t('Party'),
      'base' => 'node_content',
      'description' => t('Use this content type to create politic party'),
      'has_title' => '1',
      'title_label' => t('Party Name'),
      'help' => '',
    ),
    'proposals' => array(
      'name' => t('Proposals'),
      'base' => 'node_content',
      'description' => t('Each candidate can document his proposals for a specific area i.e education, transportation, etc.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'reference' => array(
      'name' => t('Referencia'),
      'base' => 'node_content',
      'description' => t('Use this type of content to create a reference that will be related to a party or candidate'),
      'has_title' => '1',
      'title_label' => t('Titulo'),
      'help' => '',
    ),
  );
  return $items;
}
