<?php
/**
 * @file
 * open_politic_structure.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function open_politic_structure_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'candidates_node_blocks';
  $context->description = 'Bocks Configuration for Candidates Block';
  $context->tag = 'UI';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'candidate' => 'candidate',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'open_politic_helper-contribute_reference' => array(
          'module' => 'open_politic_helper',
          'delta' => 'contribute_reference',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'open_politic_helper-compare_candidate' => array(
          'module' => 'open_politic_helper',
          'delta' => 'compare_candidate',
          'region' => 'sidebar_second',
          'weight' => '30',
        ),
        'open_politic_helper-reference_type_9' => array(
          'module' => 'open_politic_helper',
          'delta' => 'reference_type_9',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'open_politic_helper-reference_type_8' => array(
          'module' => 'open_politic_helper',
          'delta' => 'reference_type_8',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Bocks Configuration for Candidates Block');
  t('UI');
  $export['candidates_node_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'compare_candidates';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'candidate' => 'candidate',
        'party' => 'party',
        'proposals' => 'proposals',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'open_politic_helper-compare_candidate' => array(
          'module' => 'open_politic_helper',
          'delta' => 'compare_candidate',
          'region' => 'sidebar_second',
          'weight' => '30',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['compare_candidates'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'delta_candidates_party';
  $context->description = '';
  $context->tag = 'theme';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'candidate' => 'candidate',
        'party' => 'party',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'delta' => array(
      'delta_template' => 'candidates_party_delta',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('theme');
  $export['delta_candidates_party'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'party_node_blocks';
  $context->description = 'Bocks Configuration for Parties Block';
  $context->tag = 'UI';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'party' => 'party',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'open_politic_helper-contribute_reference' => array(
          'module' => 'open_politic_helper',
          'delta' => 'contribute_reference',
          'region' => 'sidebar_second',
          'weight' => '-12',
        ),
        'views-candidates_of_party-block_2' => array(
          'module' => 'views',
          'delta' => 'candidates_of_party-block_2',
          'region' => 'sidebar_second',
          'weight' => '-11',
        ),
        'views-candidates_of_party-block_1' => array(
          'module' => 'views',
          'delta' => 'candidates_of_party-block_1',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'open_politic_helper-compare_candidate' => array(
          'module' => 'open_politic_helper',
          'delta' => 'compare_candidate',
          'region' => 'sidebar_second',
          'weight' => '30',
        ),
        'open_politic_helper-reference_type_9' => array(
          'module' => 'open_politic_helper',
          'delta' => 'reference_type_9',
          'region' => 'sidebar_second',
          'weight' => '-6',
        ),
        'open_politic_helper-reference_type_8' => array(
          'module' => 'open_politic_helper',
          'delta' => 'reference_type_8',
          'region' => 'sidebar_second',
          'weight' => '-5',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Bocks Configuration for Parties Block');
  t('UI');
  $export['party_node_blocks'] = $context;

  return $export;
}
