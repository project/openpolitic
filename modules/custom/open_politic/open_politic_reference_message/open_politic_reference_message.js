(function ($) {
  var $bettertip;
  Drupal.behaviors.bettertip = {
    attach: function (context, settings) {
      var description = $('.field-name-field-related-to#edit-field-related-to .form-item .description').text();
      $('.field-name-field-related-to#edit-field-related-to .form-item .description').hide();
      var form = $('form'),
      fieldType = '.field-name-field-related-to .form-autocomplete';

      function lbl2ph(){
        form.find(fieldType).each(function(){ // loop through each field in the specified form(s)
          var el = $(this); // field that is next in line
          // add label text to field's placeholder attribute
          el.attr("placeholder",description);
        });
      }
      lbl2ph();
    }
  };
})(jQuery);
