<?php

/**
 * Implements hook_views_query_alter().
 */
function open_politic_helper_views_query_alter(&$view, &$query) {
  if ($view->name == 'versus' && $view->current_display == 'page') {
    $candidates = array();

    $candidates[] = arg(1);
    $candidates[]= arg(2);

    if(isset($candidates[0]) && is_numeric($candidates[0]) &&
     isset($candidates[1]) && is_numeric($candidates[1])) {

      //Validate parameters are candidates
      foreach ($candidates as $candidate_nid) {
        $candidate = node_load($candidate_nid);
        if($candidate->type != 'candidate') {
          return;
        }
      }
      $query->where[1]['conditions'][2]['operator'] = 'in';
      $query->where[1]['conditions'][2]['value'] = $candidates;
    }
  }
  else if ($view->name == 'like_dislike') {
    if(isset($query->table_queue['field_data_field_hierarchical_government']['join'])) {
      $query->table_queue['field_data_field_hierarchical_government']['join']->type = 'INNER';
    }

    if(isset($query->table_queue['field_data_field_location_of_government_value_0']['join'])) {
      $query->table_queue['field_data_field_location_of_government_value_0']['join']->type = 'INNER';
    }

    $query->where[1]['conditions'][3]['field'] ='votingapi_vote_node_percent.tag';
    $query->where[1]['conditions'][3]['operator'] = '=';

    if(in_array($view->current_display, array('block_1', 'block_3', 'block_5'))) {
      $query->where[1]['conditions'][3]['value'] = 'like';
    }
    else if(in_array($view->current_display, array('block_2', 'block_4', 'block_6'))) {
      $query->where[1]['conditions'][3]['value'] = 'dislike';
    }
  } else if($view->name == 'candidates_of_party' && $view->current_display == 'block_1') {
    // Exclude country term
    $query->where[1]['conditions'][3]['field'] = 'field_data_field_location_of_government.field_location_of_government_tid';
    $query->where[1]['conditions'][3]['operator'] = '<>';
    $country = taxonomy_get_term_by_name(variable_get('default_country'));
    $query->where[1]['conditions'][3]['value'] = $country[1]->tid ;
  }
}
