(function ($) {
  Drupal.behaviors.candidate_fiter = {
    attach: function (context, settings) {
      // Hide before ajax request
      $('.candidates-page #edit-field-hierarchical-government-tid').live('change',function() {
        if (this.value != 4) {
          $('.candidates-page #edit-field-location-of-government-tid').val('All');
          $('.candidates-page #edit-field-location-of-government-tid-wrapper').hide();
        } else {
          $('.candidates-page #edit-field-location-of-government-tid-wrapper').show();
        }
      });

      // Hide after ajax request
      if($('.candidates-page #edit-field-hierarchical-government-tid').val() != 4) {
        $('.candidates-page #edit-field-location-of-government-tid').val('All');
        $('.candidates-page #edit-field-location-of-government-tid-wrapper').hide();
      } else {
       $('.candidates-page #edit-field-location-of-government-tid-wrapper').show();
     }
   }
 };
}(jQuery));
