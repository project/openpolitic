<?php

/**
 * @file
 * Display Suite 2 column template.
 */
?>
<?php
  // Calculate candidat_to to set a class
  $candidate_to_class = strtolower($elements['field_hierarchical_government']['#object']->field_hierarchical_government[$elements['field_hierarchical_government']['#language']][0]['hs_lineages'][0]['label']);
  if(!empty($candidate_to_class)) {
    $classes .= " " . $candidate_to_class;
  }
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds-2col <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <<?php print $left_wrapper ?> class="group-left<?php print $left_classes; ?>">
    <?php print $left; ?>
  </<?php print $left_wrapper ?>>

  <<?php print $right_wrapper ?> class="group-right<?php print $right_classes; ?>">
    <?php print $right; ?>
  </<?php print $right_wrapper ?>>

</<?php print $layout_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
